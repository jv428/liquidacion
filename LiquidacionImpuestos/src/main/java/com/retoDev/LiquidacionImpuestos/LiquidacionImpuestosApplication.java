package com.retoDev.LiquidacionImpuestos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquidacionImpuestosApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquidacionImpuestosApplication.class, args);
	}

}
