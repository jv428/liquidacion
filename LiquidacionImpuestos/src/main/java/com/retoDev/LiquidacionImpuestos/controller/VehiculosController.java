package com.retoDev.LiquidacionImpuestos.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.retoDev.LiquidacionImpuestos.models.Vehiculos;
import com.retoDev.LiquidacionImpuestos.service.VehiculoService;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class VehiculosController {
	@Autowired
	private VehiculoService vehiculoService;
	
	@GetMapping("/getVehiculoPlaca/{placa}/{anoVigencia}")
	Vehiculos getVehiculoPlaca(@PathVariable String placa, @PathVariable String anoVigencia) {
		return vehiculoService.getVehiculoPlaca(placa, anoVigencia);
	}
	
}
