package com.retoDev.LiquidacionImpuestos.service;
import com.retoDev.LiquidacionImpuestos.models.Vehiculos;




public interface VehiculoService {

	
	Vehiculos getVehiculoPlaca(String placa, String anoVigencia);
}
