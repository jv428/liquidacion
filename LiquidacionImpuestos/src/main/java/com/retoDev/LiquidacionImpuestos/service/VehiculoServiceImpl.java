package com.retoDev.LiquidacionImpuestos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retoDev.LiquidacionImpuestos.models.Vehiculos;
import com.retoDev.LiquidacionImpuestos.repository.VehiculoRepository;

@Service
public class VehiculoServiceImpl implements VehiculoService {

	@Autowired
	private VehiculoRepository vehiculoRepository;
	private final static int VEINTE_MILLONES = 20000000;
	private final static int CUARENTA_Y_CINCO_MILLONES = 45000000;
	@Override
	public Vehiculos getVehiculoPlaca(String placa, String anoVigencia) {
		Vehiculos respuesta = vehiculoRepository.findByPlaca(placa);
		int valorVigencia = getVigencia(anoVigencia, respuesta);
		double impuesto = impuesto(respuesta, valorVigencia);
		respuesta.setImpuesto(impuesto);

		return respuesta;
	}

	private double impuesto(Vehiculos respuesta, int valorVigencia) {
		double impuesto = 0;
		int valorVehiculo = respuesta.getValorVehiculo();
		if (valorVehiculo <= VEINTE_MILLONES) {
			impuesto = valorVigencia * 0.015;
		} else if (valorVehiculo > 20000000 && valorVehiculo <= CUARENTA_Y_CINCO_MILLONES) {
			impuesto = valorVigencia * 0.025;
		} else {

			impuesto = valorVigencia * 0.035;
		}

		if (respuesta.getClase() == "Motos" && Integer.parseInt(respuesta.getCilindraje()) > 125) {
			impuesto = valorVigencia * 0.015;
		}
		return impuesto;
	}

	private int getVigencia(String anoVigencia, Vehiculos respuesta) {
		int vigencia = 0;
		switch (anoVigencia) {
		case "2015":
			vigencia = respuesta.getQuince();
			break;
		case "2016":
			vigencia = respuesta.getDieciseis();
			break;
		case "2017":
			vigencia = respuesta.getDiecisiete();
			break;
		case "2018":
			vigencia = respuesta.getDieciocho();
			break;
		case "2019":
			vigencia = respuesta.getDicinueve();
			break;
		case "2020":
			vigencia = respuesta.getVeinte();
			break;
		case "2021":
			vigencia = respuesta.getVentiuno();
			break;
		}
		return vigencia;
	}

}
