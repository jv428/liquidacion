import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LiquidacionService {

  constructor(private http: HttpClient) { }

  getPLaca(placa: string, vigencia: string) {
    return this.http.get(`http://localhost:8080/getVehiculoPlaca/${placa}/${vigencia}`);
  }

}
