import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MostarComponent } from './components/mostar/mostar.component';

const routes: Routes = [
  {path: 'mostrar', component:MostarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
