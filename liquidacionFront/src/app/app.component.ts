import { Component } from '@angular/core';
import { LiquidacionService } from '../service/liquidacion.service';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public vehiculos:any
  title = 'liquidacionFront';
  constructor(private liquidacionService: LiquidacionService, private fb: FormBuilder, private router: Router) {
  }

  formulario = this.fb.group({
    placa: ['', [Validators.required, Validators.minLength(6)]],
    vigencia: ['', [Validators.required]]
  });

  validarInfo() {
    console.log(this.formulario);
    const { placa, vigencia } = this.formulario.value
    if (!!placa && !!vigencia) {
      this.liquidacionService.getPLaca(placa, vigencia).subscribe((res) => {
        this.vehiculos = [res];
      })
    }

  }

}
